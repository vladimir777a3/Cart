<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>list.php</title>
    <style>
        * { font-size: 20px;
            font-family: Arial;
            font-weight: bold;  }
    </style>
</head>
<body>
<?php
require_once 'init.php';

$items=$object_cart->recount_cart();
$items=$object_cart->get_items();

if (($object_cart->get_sum())>0){
    echo "<table  border=\"1px\"";
    echo "<tr><td>Название</td><td>Количество</td><td>Цена</td><td>Сумма</td><td></td></tr>";
    foreach ($items as $key=>$value)
    {
        echo "<tr><td>".$items[$key]["name"]."</td><td>".$value['quantity']." шт</td><td>".$value["price"]." грн с НДС</td><td>".($value['quantity']*$value["price"])." грн с НДС</td><td><a href=\"delete.php?id_to_del=$key\">удалить</a></td></tr>";
    }
    echo "</table>";
    echo "Сумма итого: ".$object_cart->get_sum()." грн с НДС<br>";
    echo "К оплате с учетом скидки: ".$object_cart->discount_sum()." грн с НДС<br>";
}
?>
<br>
<a href="add.php">Добавить еще товаров</a>
