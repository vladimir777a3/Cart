<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>cart.php</title>
    <style>
        * {  font-size: 20px;
            font-family: Arial;
            font-weight: bold;
             }
    </style>
</head>
<body>
<?php

$products = [
    1=>['name'=>'сапоги', 'price'=>233],
    2=>['name'=>'кроссовки', 'price'=>333],
    3=>['name'=>'пальто', 'price'=>332],
    4=>['name'=>'очки', 'price'=>444],
    5=>['name'=>'шляпа', 'price'=>555],
    6=>['name'=>'цемент', 'price'=>200],
    7=>['name'=>'гвозди', 'price'=>240],
    8=>['name'=>'мотоцикл', 'price'=>1320],
    9=>['name'=>'автомобиль', 'price'=>2110],
    10=>['name'=>'стол', 'price'=>210],
    11=>['name'=>'матрас', 'price'=>310],
    12=>['name'=>'шкаф', 'price'=>450],
    13=>['name'=>'книга', 'price'=>160],
    14=>['name'=>'картина', 'price'=>180],
    34=>['name'=>'цветок', 'price'=>270],
];


class Cart
{
    // объявление свойства
    public $items;
    public $sum;
    public $discount = 1;
    public $quantity_sum = 0;
    public $discount_sum = 0;
    public $products;

    // объявление метода
    function __construct()
    {
        if (!isset($_SESSION["cart"])) {               //если массива корзины нет - создаем массив
            $_SESSION=[
                'cart'=>[
                    'sum'=>0,                             //сумма без скидки
                    "discount"=>1,                        //коэф. скидки
                    "quantity_sum"=>0,                //итого кол-во товаров
                    "discount_sum"=>0,            //сумма cо скидкой
                    'items'=>[]]];
        }
        else{                            //если корзина есть - достаем из нее все данные и раскладываем по переменным
            $this->items=$_SESSION["cart"]["items"];
            $this->sum=$_SESSION["cart"]["sum"];
            $this->discount=$_SESSION["cart"]["discount"];
            $this->quantity_sum=$_SESSION["cart"]["quantity_sum"];
            $this->discount_sum=$_SESSION["cart"]["discount_sum"];
            $this->products=$GLOBALS['products'];
            }
    }

    /**
     *
     */
    public function recount_cart() {
        $this->sum = 0;
        $this->quantity_sum = 0;         //кол-во товаров в корзине
        $this->discount = 1;                 //скидка, коэфициент к цене
        $this->discount_sum = 0;         //сумма со скидкой
        if (isset($this->items)){
            foreach ($this->items as $key => $value) {
                $this->sum = $this->sum + ($value["price"] * $value["quantity"]);  //сумма без скидки
                $this->quantity_sum = $this->quantity_sum + $value["quantity"];  //кол-во товаров в корзине
            }
        }
        if ($this->quantity_sum > 10) {
            $this->discount = 0.9;
        } else {
            if ($this->sum > 2000) {
                $this->discount = 0.93;
            }
        }
        $this->discount_sum = $this->sum * $this->discount;  //сумма cо скидкой
    }

    /**
     * @param $new_id
     * @param $new_quantity
     */
    public function add_to_cart(){
        if (isset($_GET["new_id"])and isset($_GET["new_quantity"])){      //проверяем чтоб данные были, иначе выдает ошибку
            $new_id=$_GET["new_id"];
            $new_quantity=(int)$_GET["new_quantity"];
            if ((is_int($new_quantity))and($new_quantity>0)and(isset($this->products[$new_id]))) //если из формы прилетело целое число больше 0, и товар существует, то считаем дальше
            {
                $new_price=$this->products[$new_id]['price'];        //Функция сама берет цену из базы. Покупатель не сообщает кассиру цену
                $new_name=$this->products[$new_id]['name'];
                    if (isset($this->items[$new_id]['quantity'])){
                        $total_quantity=$new_quantity+$this->items[$new_id]['quantity'];//если какое-то количество нового товара уже есть в корзине - суммируем
                    }
                    else {
                        $total_quantity=$new_quantity;
                    }
                $this->items[$new_id]=['name'=>$new_name, 'quantity'=>$total_quantity, 'price'=>$new_price];
            }
            else {echo "Количество некорректно или товар выбран не правильно";}
        }
        else {echo "Товар не выбран или количество не введено";}
}

    public function get_items()
    {
        return $this->items;
    }

    public function get_sum()
    {
        return $this->sum;
    }

    public function discount_sum()
    {
        return $this->discount_sum;
    }
    /**
     * //2. удаление товара из корзины. Удаляет $_GET['id_to_del']
     */
    public function delete_from_cart()
    {
        if (isset($_GET['id_to_del']))
        {
        $this->id_to_del=$_GET['id_to_del'];
        unset($this->items[$this->id_to_del]);
        }
        $this->recount_cart();
    }

   function __destruct()        //Сохраняем все данные в сессию
   {
        $_SESSION["cart"]["sum"]=$this->sum;
        $_SESSION["cart"]["discount"]=$this->discount;
        $_SESSION["cart"]["quantity_sum"]=$this->quantity_sum;
        $_SESSION["cart"]["discount_sum"]=$this->discount_sum;  //сумма cо скидкой
        $_SESSION["cart"]["items"]=$this->items;
   }
}

?>
</body>
</html>
