<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>add.php</title>
    <style>
        * {  font-size: 20px;
            font-family: Arial;
            font-weight: bold;
            }
    </style>
</head>
<body>

<?php
require_once 'init.php';
/*В файле содержится форма и вызов функции добавления товара в корзину.
В форме: выбор товаров из выпадающего списка select, ввод количества товара.
при отправке формы добавляем товар в корзину в сессию с помощью вызова функции из файла cart.php*/

?>
<p>Какой товар добавляем в корзину? Выберите товар и укажите его количество:</p>
<form method="get">
    Товар:<p><select name="new_id">
            <option selected disabled>Выберите товар</option>
            <?php
                foreach ($products as $key => $value) {
                    echo "<option value=\"".$key."\">".$value['name']."</option>";
                }
            ?>
        </select></p>
    <br>Количество: <input type="text" name="new_quantity">
    <p><input type="submit" name="" value="добавить в корзину"></p>

</form>

<?php
$object_cart->add_to_cart();
$object_cart->recount_cart();
echo "<br><br>";
require_once 'list.php';
?>