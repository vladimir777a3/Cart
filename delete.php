<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>delete.php</title>
    <style>
        * { font-size: 20px;
            font-family: Arial;
            font-weight: bold;  }
    </style>
</head>
<body>
<?php
require_once 'init.php';

$object_cart->delete_from_cart();

require_once 'list.php';

?>
<br>
<a href="add.php">Добавить еще товаров</a>